parpullrepo() {
    pushd "$1"; (git pull &); popd
}

pullrepo() {
    pushd "$1"; git pull; popd
}

parpullall() {
    for repo in ./*; do
        parpullrepo "$repo"
    done
    wait
}

pullall() {
    for repo in ./*; do
        pullrepo "$repo"
    done
    wait
}
