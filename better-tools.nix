{ pkgs, ... }:

let
unstable = import <nixos-unstable> {};
in
{
  environment.systemPackages = with pkgs; [
    eza #better ls
    fd #better find
    ncdu #ncurses du
    bat #better cat
    ripgrep #better grep
    bandwhich #better nettop
    httpie #better' curl
    procs #better' ps
    sd #better' sed
    broot #better' tree
    zoxide #better' cd
    bottom #better' htop
    rustscan #better' nmap
    delta #better' git diff
    bingrep
    du-dust #better du
    skim #fuzzy finder
    mhost #better dig
  ];

  environment.shellAliases =
    let
      lscmd = "${pkgs.eza.out}/bin/eza --colour-scale";
    in
      {
        ls = lscmd;
        ll = lscmd + " -l";
        l = lscmd + " -la";
        la = lscmd + " -la";
        tree = "${pkgs.broot.out}/bin/broot -s";
        du = "${pkgs.ncdu.out}/bin/ncdu --color dark";
        cat = "${pkgs.bat.out}/bin/bat";
        find = "${pkgs.fd.out}/bin/fd";
        grep = "${pkgs.ripgrep.out}/bin/rg";
        ping = "${pkgs.prettyping.out}/bin/prettyping --nolegend";
        nettop = "${pkgs.bandwhich.out}/bin/bandwhich";
        htop = "${pkgs.bottom.out}/bin/btm";
        c =  "${pkgs.zoxide.out}/bin/zoxide";
      };

  environment.interactiveShellInit =
    ''
    export MANPAGER="sh -c 'col -bx | ${pkgs.bat.out}/bin/bat -l man -p'"
    '';

  programs = {
      zsh = {
        shellAliases = {
          picsafe = "sudo mount $(ls /dev/sd* | ${pkgs.skim.out}/bin/sk -i --no-multi) ~/stick && rsync -ahP ~/stick/ ~/picsafe/ && sudo umount ~/stick";
        };
        interactiveShellInit = builtins.readFile ./nix-helper-function.zsh + builtins.readFile ./batchgit.zsh;
      };
      bandwhich.enable = true;
    };
}
