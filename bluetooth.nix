{ pkgs, ... }:

{
  hardware.bluetooth = {
    enable = true;
    package = pkgs.bluez;
  };

  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  services.blueman.enable = true;

  environment.systemPackages = with pkgs; [ blueman ];
}
