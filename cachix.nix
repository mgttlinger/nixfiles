{ pkgs, ... }:

{
  imports = [ /etc/nixos/cachix.nix ];

  environment.systemPackages = with pkgs; [
    cachix
  ];
}
