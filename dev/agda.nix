{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    (agda.withPackages (p: with p; [ agda-prelude ]))
    haskellPackages.ieee754
  ];

  environment.pathsToLink = [ "/share/agda" ];
}
