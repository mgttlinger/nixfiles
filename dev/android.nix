{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
  ];

  programs = {
    adb.enable = true;
  };

  users.users.merlin.extraGroups = [ "adbusers" ];
}
