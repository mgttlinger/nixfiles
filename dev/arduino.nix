{ pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{
  environment.systemPackages = with pkgs; [
    unstable.platformio
  ];

  users.users.merlin.extraGroups = [ "dialout" ];
}
