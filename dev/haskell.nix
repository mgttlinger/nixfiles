{ pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{
  environment.systemPackages = with pkgs; [
    haskellPackages.happy
    haskellPackages.alex
    haskellPackages.hasktags

    ormolu

    stack ghc cabal-install
  ];

}
