{ pkgs, ... }:

let
unstable = import <nixos-unstable> {};
in
{
  imports = [
    ../sw/gnupg.nix
    ../sw/pass.nix
    ../sw/nvim.nix
    ../sw/monitoring.nix
  ];
  environment.systemPackages = with pkgs; [
    nix-index nix-bundle
    uutils-coreutils
    clang
    bash man-pages sudo xsel wget curl gmp file ntfs3g
    lshw ltrace pciutils patchelf usbutils unzip zip xorg.xkill killall whois gnuplot gnumeric
    gnumake cmake
    bind

    graphviz

    # spellchecking
    hunspell languagetool hunspellDicts.de-de hunspellDicts.en-gb-ise
    ispell
    aspell aspellDicts.de aspellDicts.en aspellDicts.en-computers aspellDicts.en-science

    ffmpeg imagemagick

    albert

    #ranger and used utils
    ranger atool libcaca w3m mediainfo poppler

    # temperature monitoring
    lm_sensors s-tui stress

    shellcheck
  ];

  documentation = {
    dev.enable = true;
    doc.enable = true;
    info.enable = true;
    man = {
      enable = true;
      generateCaches = true;
    };
    nixos = {
      includeAllModules = true;
    };
  };

  programs = {
    tmux = {
      enable = true;
    };
  };
  services = {
    ntp.enable = true;

    openssh.enable = true;

    upower.enable = true;

    locate = {
      enable = true;
      interval = "hourly";
    };
  };
}
