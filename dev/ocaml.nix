{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    ocamlPackages.merlin
    ocamlPackages.ocp-indent
    ocamlPackages.ocp-index
    ocamlPackages.ocp-build
    opam
  ];
}
