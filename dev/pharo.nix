{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    pharo pharo-launcher
  ];

  environment.etc."security/limits.d/pharo.conf" = {
    source = ./pharo.conf;
  };
}
