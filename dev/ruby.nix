{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    ruby
    solargraph
    bundix
    bundler
  ];
}
