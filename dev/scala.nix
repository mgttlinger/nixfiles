{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    ammonite
    bloop
    coursier
    dotty
    mill
    sbt
    scala
    scalafmt
    scalafix
  ];
}
