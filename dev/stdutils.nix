{ pkgs, ... }:

{
  imports = [
    ./headless_stdutils.nix
    ../sw/light.nix
  ];
  environment.systemPackages = with pkgs; [
    nmap rustscan dbus-map pcmanfm

    # pdf and djvu stuff
    pandoc poppler pdftk djvulibre

    # terminals
    xterm wezterm roxterm

    deadd-notification-center libnotify
    graphviz
    # pdf view
    zathura pdfpc sioyek
    # djvu viewer
    djview

    xorg.xcursorthemes

    # photo viewers
    feh geeqie libsixel

    # screenshot
    flameshot
  ];


  services = {
    # Enable CUPS to print documents.
    printing = {
      enable = true;
      drivers = with pkgs; [ gutenprint gutenprintBin hplip splix ];
    };
  };
}
