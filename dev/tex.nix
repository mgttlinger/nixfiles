{ pkgs, ... }:

let pps = pkgs.python3Packages;
in
{
  environment.systemPackages = with pkgs; [
    texlive.combined.scheme-full
    texlab #lsp server
    pdf2svg
    pps.pygments
    arxiv-latex-cleaner
  ];
  environment.shellAliases = {
    svglatex = ''f(){ pdflatex $@ && pdf2svg $(basename ''${@: -1} .tex).pdf $(basename ''${@: -1} .tex).svg }; f'';
  };
}
