{ lib, config, options, ... }:

with lib; {
  options.kbdmap = mkOption { type = types.str; default = "de"; description = "Keyboard layout to use"; };
  config.services.xserver.xkb.layout = config.kbdmap;
  config.console.keyMap = config.kbdmap;
}
