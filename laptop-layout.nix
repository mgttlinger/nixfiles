{ config, lib, pkgs, ... }:

{
  imports =
    [ <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
    ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "usb_storage" "sd_mod" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/1e0bcfef-a5cd-4a0b-9de0-cfc422c95fce";
      fsType = "ext4";
    };
  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/53B2-08B7";
      fsType = "vfat";
    };
  fileSystems."/save" =
    { device = "/dev/disk/by-uuid/21a2addd-c9ed-4c17-9394-638ec3939181";
      fsType = "ext4";
    };
  fileSystems."/workspace" = 
    { device = "/dev/disk/by-uuid/2f76d37d-44e3-42c3-9241-f009997dc691";
      fsType = "ext4";
    };
  fileSystems."/nas" = 
    { device = "192.168.178.240:/mnt/merlinas_main";
      fsType = "nfs";
      options = [ "x-systemd.automount" "noauto" ];
    };
  swapDevices =
    [ { device = "/dev/disk/by-uuid/b07e4311-f140-4d2b-bda0-dcef5bea1d5a"; }
    ];

  nix.settings.max-jobs = lib.mkDefault 4;
  powerManagement.cpuFreqGovernor = "powersave";

  hardware.pulseaudio = {
    enable = true;
    #systemWide = true;
  };
}
