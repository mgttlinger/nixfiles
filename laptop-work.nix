{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{

  imports = [
    ./cachix.nix
    ./nix-defaults.nix
    ./os-defaults.nix
    ./os/networkmanager-wifi.nix
    ./os/gitlab-runner.nix
    ./kbd.nix
    ./efi.nix
    ./sets/gui.nix
    ./sets/security.nix
    ./sets/development.nix
    ./sets/dev-langs-base.nix
    ./sets/default-tools.nix
    ./dev/haskell-docsets.nix
    ./dev/coq.nix
    ./sw/virtualbox.nix
    ./sw/hacking.nix
    ./sw/direnv.nix
    ./sw/db.nix
    ./bluetooth.nix
    <nixos-hardware/lenovo/thinkpad/x1/6th-gen>
    ./laptop-work-layout.nix
  ];

  kbdmap = "de";
  networking.hostName = "workpad";


  environment.systemPackages = with pkgs; [
    config.boot.kernelPackages.perf
  ];




  system.stateVersion = "18.09";
}
