{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in {
  imports =
    [
      ./cachix.nix
      ./nix-defaults.nix
      ./os-defaults.nix
      ./os/networkmanager-wifi.nix
      ./kbd.nix
      ./efi.nix
      ./servers.nix
      ./sets/gui.nix
      ./sets/security.nix
      ./sets/development.nix
      ./sets/dev-langs-base.nix
      ./sets/default-tools.nix
      ./dev/ocaml.nix
      ./sw/hacking.nix
      ./sw/db.nix
      ./laptop-layout.nix
    ];

  kbdmap = "de";
  networking.hostName = "hackpad";


  # List packages installed in system profile.
  environment.systemPackages = with pkgs; [

    keepass
  ];

  programs = {
    sysdig.enable = true;
  };


  nixpkgs.config = {
    packageOverrides = pkgs: {
      #emacs = pkgs.emacs.override {
      #  withXwidgets = true;
      #};
    };
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;


  # List services that you want to enable:

  services = {
    postgresql = {
      enable = true;
      dataDir = "/workspace/phd/postgresdata";
      package = pkgs.postgresql_10;
    };
  };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "18.09";
}
