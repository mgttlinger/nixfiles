{ pkgs, ... }:

{
  hardware.logitech = {
    enable = true;
    enableGraphical = true;
  };
}
