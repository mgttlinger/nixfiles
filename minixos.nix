{ config, pkgs, ... }:

let
unstable = import <nixos-unstable> {};
in
{
  imports = [
    ./cachix.nix
    ./nix-defaults.nix
    ./os-defaults.nix
    ./os/networkmanager-wifi.nix
    ./kbd.nix
    ./efi.nix
    ./sets/gui.nix
    ./sets/security.nix
    ./sets/development.nix
    ./sets/dev-langs-base.nix
    ./sets/default-tools.nix
    ./minixos-layout.nix
    ./bluetooth.nix
  ];

  kbdmap = "us";
  boot.kernelParams = [ "fbcon=rotate:3" ];

  networking.hostName = "minixos";

  services = {
    xserver = {
      dpi = 160; #actually about 360 but that is waaaay too large
      monitorSection = ''
        Option "Rotate" "left"
      '';
      xkbOptions = "compose:menu";
      wacom.enable = true;
    };
    fprintd.enable = true;
  };

  systemd.user.services.hiio =
    let
      hiio = import /workspace/hiio/shell.nix {};
    in {
    description = "hIIO";
    enable = true;
    path = with pkgs.xorg; [ xrandr xinput ];
    serviceConfig = {
      ExecStart = "${hiio.out}/bin/hiio -o eDP-1 -i '[15, 16]' -d -f";
      ExecReload = "${pkgs.coreutils.out}/bin/kill -HUP $MAINPID";
      PassEnvironment = "DISPLAY";
      KillMode = "control-group"; # upstream recommends process
      Restart = "on-failure";
      PrivateTmp = true;
      ProtectSystem = "full";
      Nice = 10;
    };
    after = ["graphical.target"];
    wantedBy = ["graphical.target"];
  };

  environment.systemPackages = with pkgs; [
  ];


  system.stateVersion = "19.09";
}
