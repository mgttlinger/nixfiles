{
  nixpkgs.config = {
    allowUnfree = true;
  };

  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  nix = {
    settings = {
      sandbox = true;
      trusted-users = [ "root" "merlin" ];
      auto-optimise-store = true;
      substituters = [ "https://cache.nixos.org/" "https://nixcache.reflex-frp.org" ];
      trusted-public-keys = [ "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=" ];
    };

    checkConfig = true;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 60d";
    };
  };
}
