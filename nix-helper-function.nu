def nix-clean-gc-auto-roots [] {
    enter /nix/var/nix/gcroots/auto/
    try {
        for $root in (ls -l | where type == symlink | sort-by target -r) {
            let prompt = ["Delete ", $root.target, "? [y/N] "] | str join
            match (input --numchar 1 $prompt) {
              "y" => { sudo rm $root.target },
              "Y" => { sudo rm $root.target },
               _ => { continue }
            }
        }
    }
    dexit
}

def nix-free [] {
    nix-clean-gc-auto-roots
    nix-collect-garbage --delete-older-than 100d
    sudo nix-collect-garbage --delete-older-than 100d
}

def nix-upd [] {
    sudo nix-channel --update
    nix profile upgrade '.*' --impure o+e>| nom
    nixos-rebuild --upgrade switch o+e>| nom
}