nix-clean-gc-auto-roots () {
    pushd /nix/var/nix/gcroots/auto/
    for root in $(ls -l | awk -F  "-> " '{} {print $2}' | sort -r)
    do
        [ -L $root ] && [ -e $root ] && echo "Delete $root? [y/N] " && read -q && sudo rm "$root"
    done
    popd
}

nix () {
    case "$1" in
        tbuild)
            pushd "/workspace/nixpkgs/"
            command nix-build . -A "${@:2}"
            popd
            ;;
        brun)
            command nix build && "./result/bin/$2" "${@:3}"
            ;;
        env)
            command nix-env "${@:2}"
            ;;
        upd)
            sudo nix-channel --update && nix profile upgrade '.*' --impure |& nom && sudo nixos-rebuild --upgrade switch |& nom
            ;;
        free)
            nix-clean-gc-auto-roots && nix-collect-garbage --delete-older-than 100d && sudo nix-collect-garbage --delete-older-than 100d
            ;;
        cbuild)
            command nix-build "${@:2}" | cachix push -w mgttlinger
            ;;
        help)
            which nix
            ;;
        watch)
            echo "Watching directory"
            while inotifywait -q -q -e create -e modify -e delete -r . @result --exclude '(\.\#.*)|(\#.*\#)'; do
                clear
                nix-build && "${@:2}"
            done
            ;;
        git)
            pushd "/workspace/nixfiles/"
            git "${@:2}"
            popd
            ;;
        *)
            command nix "$@"
            ;;
    esac
}
