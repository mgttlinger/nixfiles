{ config, pkgs, lib, ... }:
{ 
 imports = [
    ./sets/dev-langs-base.nix
    ./sets/default-tools-headless.nix
    ./sw/vcs.nix
    ./sw/zsh.nix
    ./nix-defaults.nix
    ./better-tools.nix
    ];

    services.sshd.enable = true;

    environment.systemPackages = with pkgs; [
      emacs neovim 
    ];

    users.users.merlin = {
      isNormalUser = true;
      home = "/home/merlin";
      description = "Merlin";
      extraGroups = [ "wheel" ];
      shell = pkgs.zsh;
      openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDuT2aEamTIEoucxEU6FgYw/srvua/3fBbUH6fEfkCzHJqkBtCMOp9Qi0wwnYIwfmlKhPVnnuNaHhlRYVAGKkxSg2MR3acZJsEc2juCZwmi4PrwK1ZwHHP1yvTYybzJr8D/glPxmch163hkn/q0Qx5gfdRjsLZiUonrZbzxnwUDe/rXkx46d0aCDZPRKj85U4BcwqBTXyUWuIIwetyeLWYUrAQ3ozSiii5tGUBM0gOf1iC3aeXsDqxnb5llzLzm997rJkPZsdZKbgWX+ssIO4nylgfK/hvH0W9KrDq0UJ/1Toz6cAh/N7vwSrMz8/L2fD2qyKtaTc9fKjeXZd68AzBB merlin@hackpad" ];
    };

    networking.firewall.allowedTCPPorts = [ 22 80 443 ];
    networking.firewall.allowedUDPPorts = [ ];

    system.stateVersion = "24.11";
}
