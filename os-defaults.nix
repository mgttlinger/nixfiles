{ pkgs, stdenv, ... }:

let playwrite = pkgs.stdenv.mkDerivation rec {
  pname = "playwrite";
  version = "b5a056e";

  src = pkgs.fetchFromGitHub {
    owner = "TypeTogether";
    repo = "Playwrite";
    rev = "${version}";
    hash = "sha256-XhCMhRk4lSVCy6k3KzuY5Wu7wgjwFsbepjTJkYy50+Q=";
  };

  installPhase = ''
    mkdir -p $out/share/fonts/truetype
    cp -v fonts/ttf/*.ttf $out/share/fonts/truetype
  '';
}; 
in

{
  imports = [ ./servers.nix
              ./dfn2-cert.nix
            ];

  environment.shellAliases = {
    ":q" = "exit";
  };

  environment.variables = {
    TERMINAL = "${pkgs.wezterm}/bin/wezterm";
    BROWSER = "${pkgs.chromium}/bin/chromium";
  };
  environment.homeBinInPath = true;
  programs.starship = {
    enable = true;
    settings = {};
  };

  networking.enableIPv6 = false;

  users.users.merlin = {
    isNormalUser = true;
    createHome = true;
    home = "/home/merlin/";
    description = "Merlin Humml";
    extraGroups = [ "pcsc_scan" "wheel" ];
    shell = pkgs.nushell;
    uid = 1000;
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDuT2aEamTIEoucxEU6FgYw/srvua/3fBbUH6fEfkCzHJqkBtCMOp9Qi0wwnYIwfmlKhPVnnuNaHhlRYVAGKkxSg2MR3acZJsEc2juCZwmi4PrwK1ZwHHP1yvTYybzJr8D/glPxmch163hkn/q0Qx5gfdRjsLZiUonrZbzxnwUDe/rXkx46d0aCDZPRKj85U4BcwqBTXyUWuIIwetyeLWYUrAQ3ozSiii5tGUBM0gOf1iC3aeXsDqxnb5llzLzm997rJkPZsdZKbgWX+ssIO4nylgfK/hvH0W9KrDq0UJ/1Toz6cAh/N7vwSrMz8/L2fD2qyKtaTc9fKjeXZd68AzBB merlin@hackpad"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDGU7UeGEFbWX4kF74zCsrcVsqN+PcWUsNE679rfw1XQFUeh19KJwfvQA234uIKr+9m951MKU02evb4QU3QGiPRLRfce5dgUlv+RRQEZeEHvo0+B0m76kNffRCNsDPJdp0SUwsTbg5OHIdMJU4/ExUz0VnJJtk6hV3Jgg8DvHFdXbLwCOQN3KgPtsoRWuLBspvEZdU+dyP0hltY5ndK3t5G3HQxtasWa635k3Ry9IKl0bRVOp7KjkDKZ0tRZ6rTr6nofukvUgo3SZbDyXMiR6b2lvsidoU8wsDZNno6r7z2SFp37VAI2uu/O6eBcD1zHAs/niPFzsqke1TMmCB+irx9 cardno:000607019262"
    ];
  };

  fonts = {
    packages = with pkgs; [
      source-code-pro
      source-sans-pro
      source-serif-pro
      powerline-fonts
      comic-neue
      vistafonts
      corefonts
      playwrite
    ];
  };

  services.udev.extraRules = ''ACTION=="add", SUBSYSTEM=="video4linux", DRIVERS=="uvcvideo", RUN+="${pkgs.v4l-utils.out}/bin/v4l2-ctl --set-ctrl=power_line_frequency=1"'';

  console.font = "Lat2-Terminus16";
  i18n = {
    defaultLocale = "en_GB.UTF-8";
  };

  time.timeZone = "Europe/Berlin";

}
