{ pkgs, ... }:

{
  services.gitlab-runner = {
    enable = true;
    gracefulTermination = true;
    gracefulTimeout = "3min";
    extraPackages = with pkgs; [ su bash ];
    settings.concurrent = 4;
    services = {
      dockerGit8 = {
        registrationConfigFile = "/data/secrets/git8-runner-registration";
        executor = "docker";
        tagList = [ "docker" "merlin" ];
        dockerImage = "alpine:latest";
      };
      nixGitlab = {
        registrationConfigFile = "/data/secrets/nix-runner-registration";
        executor = "shell";
        tagList = [ "nix" ];
      };
    };
  };
}
