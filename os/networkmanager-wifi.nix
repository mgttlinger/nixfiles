{ pkgs, ... }:

{
  networking = {
    wireless.enable = false;
    networkmanager.enable = true;
  };

  users.users.merlin.extraGroups = [ "networkmanager" ];
}
