{config, ...}:

{
  networking = {
    hostName = "pong";
    nameservers = [ "131.188.0.10"
	            "131.188.0.11"
		  ];
    defaultGateway = {
      address = "10.188.48.1";
      interface = "ens32";
    };
    dhcpcd.enable = false;
    domain = "informatik.uni-erlangen.de";

    interfaces.ens32 = {
      ipv4.addresses = [
        { address = "10.188.48.101";
          prefixLength = 25;
        }
      ];
    };
  };
}
