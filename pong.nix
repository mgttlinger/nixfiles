{ config, pkgs, lib, ... }:
{ 
 imports = [
    ./sets/dev-langs-base.nix
    ./sets/default-tools-headless.nix
    ./sw/vcs.nix
    ./sw/zsh.nix
    ./sw/docker.nix
    ./nix-defaults.nix
    ./better-tools.nix
    ./pong-layout.nix
    ./pong-network.nix
    ];

    boot.loader.grub.device = "/dev/sda";


    services.sshd.enable = true;

    virtualisation.vmware.guest = {
      enable = true;
      headless = true;
    };

    services.openssh.settings.PermitRootLogin = lib.mkDefault "yes";
    services.getty.autologinUser = lib.mkDefault "root";

    environment.systemPackages = with pkgs; [
      gitlab-runner
      emacs neovim 
      texlive.combined.scheme-full
    ];

   #services.whitebophir = {
   #  enable = true;
   #  port = 8080;
   #};

    users.users.merlin = {
      isNormalUser = true;
      home = "/home/merlin";
      description = "Merlin";
      extraGroups = [ "wheel" ];
      shell = pkgs.zsh;
      openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDuT2aEamTIEoucxEU6FgYw/srvua/3fBbUH6fEfkCzHJqkBtCMOp9Qi0wwnYIwfmlKhPVnnuNaHhlRYVAGKkxSg2MR3acZJsEc2juCZwmi4PrwK1ZwHHP1yvTYybzJr8D/glPxmch163hkn/q0Qx5gfdRjsLZiUonrZbzxnwUDe/rXkx46d0aCDZPRKj85U4BcwqBTXyUWuIIwetyeLWYUrAQ3ozSiii5tGUBM0gOf1iC3aeXsDqxnb5llzLzm997rJkPZsdZKbgWX+ssIO4nylgfK/hvH0W9KrDq0UJ/1Toz6cAh/N7vwSrMz8/L2fD2qyKtaTc9fKjeXZd68AzBB merlin@hackpad" ];
    };

    users.users.localadmin = {
      isNormalUser = true;
      home = "/home/localadmin";
      description = "Johanna";
      extraGroups = [ "wheel" ];
      shell = pkgs.bash;
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAqbbWR83qzAp/qp7Zb8jmKOptvjfL0DrPshtA15Fl8GuWJ62XuOfpYuJcDeXoVlDPUWHGCasgPFLZoREUEeyO0ws8AXtpVXdZxTXUJbdG2DgXW7wwx6iaStplwjTZzZEBqj6ni8hbyQgGb1gKLln+ntj1m9swXAVkwif0CFYc6BM6slqU+WaVLQmpioSqx0UgFLtEOrVecqbidtJBEJMe027iF61BFE2yusDKjZZGxGNCMmOILTrR+YiSMq/E+i7wR0CtaVZZ4cqW907aZUhUJmQZ9rcTnCZmukOYsKuZLioc5cdXYFYbac77lzUKRrxJAUHu+K13DWPccb6l4YOd1Q== vittinghoff"
        "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAmNNsFsNFHPwal6EcqQMhU8KjKndbznNsVfyQc6cPkSgDvTSJ6g6JbZRQu+1VN6XCnFrZhD4j59ZOyZHqDZ8zNNA7hqXZs6LdAtXllxtMQlq2lO7HMs9fxiuRk/ejjbtAiV/+SZWsPCbDIholNaACT0HuQ5DVRP2L765De+0qu2CQU9sAFIEBCOLNXoUGMlH34NqTs6R8XpPKK5VrfBFprFwV0ZJgG/JxJy7Lt5k0g+zZWkX7Mgmj50qw1hTiVZSrKU07PN3K5SpSZFAloEn7J8+QJafS3M/8TPzbZMVRHlB17aIa1skydDfh/q6S1iaY+HX36bfPw/sCvpXaZ5N2jw=="
      ];
    };

    #users.users.prtguser = {
    #  isNormalUser = false;
    #  isSystemUser = true;
    #  shell = pkgs.bash;
    #  description = "User for monitoring";
    #};

    #services.gitlab = {
    #  enable = false;
    #  host = "pong.informatik.uni-erlangen.de";
    #  port = 80;
    #  https = false;
    #  initialRootPasswordFile = "/var/lib/gitlab/initrootpw";
    #  secrets = let sec = "/var/lib/gitlab/secret.pem"; in {
    #    dbFile = sec;
    #    jwsFile = sec;
    #    otpFile = sec;
    #    secretFile = sec;
    #  };
    #};

    #virtualisation.oci-containers.containers.grakn = {
    #  image = "graknlabs/grakn:1.8.4";
    #  ports = [ "1729:48555" ];
    #  volumes = [ "/grakn/data/:/opt/grakn-core-all-linux/server/data/" ];
    #};

    #virtualisation.oci-containers.containers.webvowl = {
    #  image = "mmiklas/webvowl:latest";
    #  ports = [ "8080:8080" ];
    #  volumes = [ "/webvowl/data:/data" ];
    #};

    #services.neo4j = {
    #  enable = true;
    #  defaultListenAddress = "0.0.0.0";
    #};

    networking.firewall.allowedTCPPorts = [ 80 8080 5432 443 8193 1729 ];
    networking.firewall.allowedUDPPorts = [ 161 162 ];


    #systemd.services.snmpd = let
    #  snmpconfig = pkgs.writeTextFile {
    #    name = "snmpd.conf";
    #    text = ''
    #      agentAddress udp:0.0.0.0:161
    #                                             
    #      view   systemonly  included   .1.3.6.1.2.1.1
    #      view   systemonly  included   .1.3.6.1.2.1.25.1
    #      view   all         included   .1                80
#
    #      rocommunity tcsde                                **
    #      rocommunity tcsde default    -V systemonly
    #                                            
    #      rocommunity tcsde default -V systemonly                                                                                       
    #      rouser   authOnlyUser
    #                                  
    #      sysLocation    Sitting on the Dock of the Bay
    #      sysContact     Me <me@example.org>
    #      sysName         SystemName
    #      sysServices    72
    #      sysobjectid 1.3.6.1.4.1.8072.3.2.16
#
#
    #      proc  mountd                              # No more than 4 'ntalkd' processes - 0 is OK
    #      proc  ntalkd    4                               # At least one 'sendmail' process, but no more than 10
    #      proc  sendmail 10 1
    #      disk       /     10000
    #      disk       /var  5%
    #      includeAllDisks  10%
    #      load   12 10 5
    #       trapsink     localhost public
    #                                #   send SNMPv2c traps
    #      #trap2sink    localhost public
    #                                #   send SNMPv2c INFORMs
    #      #informsink   localhost public
    #      iquerySecName   internalUser
    #      rouser          internalUser
    #                               # generate traps on UCD error conditions
    #      defaultMonitors          yes
    #                               # generate traps on linkUp/Down
    #      linkUpDownNotifications  yes
    #       extend    test1   /bin/echo  Hello, world!
    #       extend-sh test2   echo Hello, world! ; echo Hi there ; exit 35
    #      #extend-sh test3   /bin/sh /tmp/shtest
    #       master          agentx
    #                                       #  Listen for network connections (from localhost)
    #                                       #    rather than the default named socket /var/agentx/master
    #      #agentXSocket    tcp:localhost:705
    #      rwuser bootstrap priv
    #      rwuser demo priv
    #    '';
    #  };
    #in {
    #  description = "net-snmp daemon";
    #  wantedBy = [ "multi-user.target" ];
    #  serviceConfig = {
    #    ExecStart = "${pkgs.net_snmp}/bin/snmpd -f -c ${snmpconfig}";
    #    KillMode = "process";
    #    Restart = "always";
    #  };
    #};

    #services.nginx = let fqdn = config.networking.hostName + config.networking.domain; in {
    #  enable = false;
    #  recommendedGzipSettings = true;
    #  recommendedOptimisation = true;
    #  recommendedProxySettings = true;
    #  recommendedTlsSettings = true;
    #  virtualHosts."${fqdn}" = {
    #    enableACME = false;
    #    forceSSL = false;
    #    locations."/".proxyPass = "http://unix:/run/gitlab/gitlab-workhorse.socket";
	# dosn't work for some reason... locations."/riot/".root = pkgs.riot-web;
    #  };
    #};

    # services.postgresql = {
    #   enable = true;
    #   enableTCPIP = true;
    #   ensureDatabases = [ "patterns" ];
    #   ensureUsers = [
    #     { name = "merlin";
    #       ensurePermissions = {
    #         "DATABASE patterns" = "ALL PRIVILEGES";
    #       };
    #     }
    #     { name = "philipp";
    #       ensurePermissions = {
    #         "DATABASE patterns" = "ALL PRIVILEGES";
    #       };
    #     }
    #     { name = "natalie";
    #       ensurePermissions = {
    #         "DATABASE patterns" = "ALL PRIVILEGES";
    #       };
    #     }
    #   ];
    # };

    services.gitlab-runner =  {
      enable = true;
      gracefulTermination = true;
      gracefulTimeout = "3min";
      settings = {
        concurrent = 4;
        sessionServer = {
          sessionTimeout = 1800;
          #listenAddress = "[::]:8193";
          #advertiseAddress = "pong.informatik.uni-erlangen.de:8193";
        };
      };
      services = {
        nix = {
          registrationConfigFile = "/data/secrets/gitlab-runner-registration";
          executor = "shell";
          tagList = [ "nix" "emacs" "texlive" ];
        };
      };
      extraPackages = with pkgs; [ su bash ];
    };

    system.stateVersion = "20.09";
}
