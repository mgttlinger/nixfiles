{ pkgs, ... }:

{
  imports = [
    ../sw/avahi.nix
    ../better-tools.nix
    ../sw/surf.nix
  ];

  environment.systemPackages = with pkgs; [
    # nix tooling
    nixpkgs-review nix-output-monitor niv manix statix nil
    # shell tooling
    starship
    # editors
    helix
    # shell tools
    jacinda jq
    # control tools
    playerctl
  ];

}
