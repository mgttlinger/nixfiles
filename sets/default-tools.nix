{ pkgs, ...}:
{
  imports = [
    ./default-tools-headless.nix
    ../sw/communication.nix
    ../sw/entertain.nix
  ];

  environment.systemPackages = with pkgs; [
    flameshot
    xorg.xev
    pavucontrol
    sioyek
  ];
}
