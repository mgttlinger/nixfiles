{
  imports = [
    ../dev/haskell.nix
    # ../dev/idris.nix
    ../dev/tex.nix
    # ../dev/lean.nix
  ];
}
