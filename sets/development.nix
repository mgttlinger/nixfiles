{
  imports = [
    ../dev/stdutils.nix
    ../sw/docker.nix
    ../sw/emacs.nix
    ../sw/vcs.nix
    ../sw/zsh.nix
  ];
}
