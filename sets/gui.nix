{
  imports = [
    ../sw/x.nix
    ../sw/xmonad.nix
    ../sw/autorandr.nix
    ../sw/redshift.nix
    ../sw/slock.nix
    ../sw/numix.nix
  ];
}
