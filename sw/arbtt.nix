{ pkgs, ... }:

let
  arbtt = pkgs.haskellPackages.arbtt;
in
{
  environment.systemPackages = with pkgs; [
    arbtt
  ];

  services.arbtt = {
    enable = true;
    package = arbtt;
  };
}
