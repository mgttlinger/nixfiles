{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    autorandr
  ];

  services = {
    autorandr = {
      enable = true;
    };
  };
}
