{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    libbluray libaacs makemkv handbrake
  ];

  boot.kernelModules = [ "sg" ];
}
