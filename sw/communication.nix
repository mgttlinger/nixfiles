{ pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{
  environment.systemPackages = with pkgs; [
    chromium

    mu isync aerc aba afew notmuch

    unstable.tdesktop

    element-desktop

    obs-studio
  ];

  programs = {
    chromium = {
      enable = true;
    };
  };


  systemd.user.services = {
    isync-sync = {
      description = "mail sync";
      enable = true;
      path = with pkgs; [ pass bash ];
      requisite = ["dev-yubikey.device"];
      after = ["dev-yubikey.device"];
      serviceConfig = {
        ExecStart = "${pkgs.isync.out}/bin/mbsync -Va";
        ExecStartPost = "${pkgs.notmuch.out}/bin/notmuch new";
        ExecReload = "${pkgs.coreutils.out}/bin/kill -HUP $MAINPID";
        Type = "oneshot";
        PrivateTmp = true;
        ProtectSystem = "full";
        Nice = 10;
      };
      wantedBy = ["default.target"];
    };
  };
  systemd.user.timers = {
    isync-sync-timer = {
      description = "trigger for mail sync";
      enable = true;
      timerConfig = {
        OnBootSec = "2m";
        OnUnitActiveSec = "2m";
        Unit = "isync-sync.service";
      };
      wantedBy = ["timers.target"];
    };
  };
}
