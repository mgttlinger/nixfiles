{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [ direnv nix-direnv ];

  environment.pathsToLink = [
    "/share/nix-direnv"
  ];

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
}
