{ pkgs, ... }:

let
  border = builtins.fetchurl {
    url = "https://tools.suckless.org/dmenu/patches/border/dmenu-border-4.9.diff";
  };
  center = builtins.fetchurl {
    url = "https://tools.suckless.org/dmenu/patches/center/dmenu-center-20191105-f1ca0d0.diff";
  };
  fuzzy = builtins.fetchurl {
    url = "https://tools.suckless.org/dmenu/patches/fuzzymatch/dmenu-fuzzymatch-4.9.diff";
  };
  color = builtins.fetchurl {
    url = "https://tools.suckless.org/dmenu/patches/morecolor/dmenu-morecolor-20190922-4bf895b.diff";
  };
  prefix = builtins.fetchurl {
    url = "https://tools.suckless.org/dmenu/patches/prefix-completion/dmenu-prefixcompletion-4.9.diff";
  };
in
{
  environment.systemPackages = with pkgs; [
    dmenu
  ];

  nixpkgs.config.dmenu.patches = [
    border center fuzzy color prefix
  ];

  # programs.zsh.shellAliases = {
  #   dmenu = "${pkgs.dmenu.out}/bin/dmenu";
  # };

}
