(custom-set-variables '(inhibit-startup-screen t) ;I know what I'm running
                      '(column-number-mode t) ;display column number in modeline
                      '(package-check-signature nil)
                      '(doc-view-continuous t) ;continuous scrolling in docview
                      '(prettify-symbols-unprettify-at-point 'right-edge) ;unprettify symbols when cursor is close
                      '(backup-directory-alist '(("." . "~/.emacs.d/backups"))) ;dont pollute things with backup files but store them globally instead
                      '(custom-file (make-temp-file "custom")) ;use a temporary custom file to not persist settings
                      '(tags-add-tables nil)                   ;don't bother me with tags prompts
                      '(indent-tabs-mode nil)                  ;indent with spaces
                      '(tab-width 4)                           ;a tab should be 4 spaces
                      )
;; disable sleep
(global-unset-key (kbd "C-z"))
;; free keybindings
(global-unset-key (kbd "C-x s"))
(global-unset-key (kbd "C-f"))

(require 'package)

(when (not package-archive-contents)
  (package-refresh-contents))

(require 'use-package)

(use-package advice-patch)  ;; Use patches to advise the inside of functions
(use-package xr) ;; Tool to convert regex to rx
(use-package xref) ;; Cross-referencing commands
(use-package tramp) ;; Transparent Remote Access, Multiple Protocol
(use-package alert ;; For DBus alerts
  :custom (alert-default-style 'libnotify "Use DBus notifications"))
(use-package vterm
  :bind ("C-x C-x" . vterm))

(use-package realgud :disabled) ;; Modular debugger frontend
(use-package project) ;; Operations on the current project
(use-package font-lock-studio)

(use-package gnu-elpa-keyring-update) ;; Update gnu elpa signatures

(defmacro byteload (path) "Load the given file and recompile if necessary"
          `(progn (byte-recompile-file (concat ,path ".el") nil 0) (load ,path)))

(defun find-in-nix-store (executable)
  "Returns the path of the current package in the nix store"
  (let* ((ep (shell-command-to-string (concat "readlink $(which " executable ")"))))
    (file-name-as-directory (replace-regexp-in-string "/bin.*[\r\n]*.*" "" ep))))

(defun name-ln (name)
  "Returns the last name of a bibtex author"
  (string-trim (if name
                   (let ((p (s-split "," name)))
                     (if (eq 1 (length p)) (-last-item (s-split " +" (car p) t)) (car p)))
                 "")))

(defun name-fn (name)
  "Returns the first name of a bibtex author"
  (string-trim (if name
                   (let ((p (s-split "," name)))
                     (if (eq 1 (length p)) (s-join " " (nbutlast (s-split " +" (car p) t))) (-last-item p)))
                 "")))

(defun author-format (author)
  "Formats the name of an author. Merlin Göttlinger -> M-Göttlinger"
  (progn
    (setq case-fold-search nil)
    (if author
        (replace-regexp-in-string "[{}.]" ""
                                  (replace-regexp-in-string "[ ]+" "-"
                                                            (let ((fn (name-fn author))
                                                                  (ln (name-ln author)))
                                                              (s-join "-" (list (replace-regexp-in-string "[[:lower:]-]+" "" fn) ln)))))
      "")))

(defun authors-format (authors)
  "Convents a bibtex author list to printable format"
  (if authors
      (s-join "_" (mapcar 'author-format (s-split " and " authors)))
    ""))

(defun escape-symbols (input)
  "remove illegal symbols from filename"
  (replace-regexp-in-string "[, &]+" "-" (replace-regexp-in-string "[ ]*[:]+[ ]*" "_" (replace-regexp-in-string "[?!$.’{}/]" "" input))))

(defun merlin-sla (name)
  ""
  (interactive "Mname:")
  (helm :sources 'helm-source-occur
        :buffer "*merlin sla*"
        :input (concat "[^[:alpha:]]" name "[^[:alpha:]]")))

(use-package auth-source-pass ;; allows emacs to automatically query pass for passwords and usernames"
  :config
  (auth-source-pass-enable))

(use-package direnv
  :config
  (advice-add 'lsp :before #'direnv-update-environment)
  (direnv-mode))

(use-package which-key ;; Display keybindings under currently types prefix
  :config (which-key-mode))
(use-package visual-regexp ;; Pretty pretty regexp replace
  :bind ("M-C-%" . vr/query-replace))
(use-package ws-butler ;; Trim only edited lines
  :hook prog-mode-hook)

(use-package wgrep) ;; TODO: learn and document

(use-package ediff
  :custom
  (ediff-window-setup-function #'ediff-setup-windows-plain)
  (ediff-split-window-function #'split-window-horizontally))

(use-package flymake)
(use-package ispell
  :custom
  (ispell-dictionary "british-ise"))
(use-package flyspell
  :hook ((text-mode-hook . flyspell-mode)
         (prog-mode-hook . flyspell-prog-mode)))

(use-package doc-view
  :hook
  (doc-view-mode-hook . #'auto-revert-mode))

(use-package org
  :bind (("C-x x" . org-capture)
         ("C-x c a" . org-agenda-list)
         ("C-x c t" . org-todo-list)
         :map org-mode-map
         ("M-." . org-open-at-point)
         ("M-," . org-mark-ring-goto)
         ("C-c C-a" . org-latex-export-to-pdf))        
  :init
  (defun transform-square-brackets-to-round-ones(string-to-transform)
    "Transforms [ into ( and ] into ), other chars left unchanged."
    (concat
     (mapcar #'(lambda (c) (if (equal c ?\[) ?\( (if (equal c ?\]) ?\) c))) string-to-transform)))
  :custom
  (org-confirm-babel-evaluate nil "Dont bother me when evaluating. Unsafe for obvious reasons!")
  (org-latex-listings 'minted "Use minted for highlighting. Requires pygments to be in PATH")
  (org-latex-pdf-process '("latexmk -pdflua -f -shell-escape -quiet %f") "use latexmk for export. -shell-escape necessary because of minted calling external executables")
  :config
  (use-package org-fragtog ;; Automatically toggling org latex fragements
    :after org
    :hook org-mode-hook)
  (use-package ob
    :custom
    (org-babel-load-languages . '((emacs-lisp . t)(python . t)(shell . t)))))

(use-package ox-beamer
  :bind (:map org-beamer-mode-map
        ("C-c C-a" . org-beamer-export-to-pdf)))

(use-package ob-prolog
  :after org
  :config (org-babel-do-load-languages 'org-babel-load-languages '((prolog . t))))

;; Hide unnecessary UI stuff
(scroll-bar-mode -1)
(tool-bar-mode   -1)
(menu-bar-mode   -1)

;; Prettify symbols everywhere
(global-prettify-symbols-mode)

(use-package line-numbers ;; Line numbers in all programmy modes
  :hook (prog-mode-hook . display-line-numbers-mode))

;; Prevent dark cursor over dark emacs
(add-to-list 'after-make-frame-functions '(lambda (fr)
                                            (with-selected-frame fr
                                              (set-mouse-color "LightBlue1")
                                              (custom-set-faces
                                               '(mode-line ((t (:foreground "#c6c6c6" :background "#404040"))))
                                               '(mode-line-buffer-id ((t (:foreground "#c6c6c6" :background "#555555"))))))))

(custom-set-faces
 '(default ((t (:slant normal :weight normal :height 120 :width normal :family "Hack")))))

(use-package moe-theme ;; Pretty dark theme
  :config
  (moe-dark)
  :custom-face
  (mode-line . '((t (:foreground "#c6c6c6" :background "#404040"))))
  (mode-line-buffer-id . '((t (:foreground "#c6c6c6" :background "#555555")))))

;; Display file size in status bar
(size-indication-mode)
(use-package all-the-icons ;; Icon fonts for modeline
  :config
  (all-the-icons-install-fonts t))

(use-package doom-modeline ;; Prettier modeline
  :custom
  (doom-modeline-buffer-file-name-style 'truncate-with-project)
  (doom-modeline-unicode-falback        t)
  (doom-modeline-minor-modes            t)
  (doom-modeline-mu4e                   t)
  (doom-modeline-gnus                   nil)
  (doom-modeline-irc                    nil)
  (doom-modeline-env-version            nil)
  (doom-modeline-icon                   'display-graphic-p)
  (all-the-icons-color-icons            t)
  :config
  (doom-modeline-mode))

(use-package minions ;; Minor modes as menu
  :config (minions-mode))

(use-package autorevert)
(use-package eldoc)

(use-package paren
  :custom
  (show-paren-style . 'mixed)
  :config (show-paren-mode))
(use-package rainbow-delimiters ;; Color balanced expressions
  :hook prog-mode-hook)

(use-package helpful ;; Prettier help menus
  :bind (("C-h f" . helpful-callable)
         ("C-h v" . helpful-variable)
         ("C-h k" . helpful-key)
         ("C-h ." . helpful-at-point)))

(use-package helm
  :custom
  (helm-mode-fuzzy-match                 t)
  (helm-completion-in-region-fuzzy-match t)
  (helm-autoresize-mode                  t)
  (helm-mode                             t)
  :config
  (use-package helm-bibtex ;; bibliography management with helm interface
    :after helm
    :bind (("C-x l" . helm-bibtex))
    :custom
    (bibtex-completion-cite-prompt-for-optional-arguments nil "Pre and postnotes are not common in CS")
    (bibtex-completion-cite-commands '("cite" "textcite" "citet" "citeauthor" "citetitle"))
    (bibtex-completion-pdf-field "file")
    (bibtex-completion-library-path '("/literature"
                                        "/workspace/Argumentation-SPP/literature/articles"
                                        "/workspace/Argumentation-SPP/literature/books"
                                        "/workspace/reg-ass/literature"))
    (bibtex-completion-bibliography '("/literature/litman.bib"
                                        "/workspace/Argumentation-SPP/literature/rant.bib"
                                        "/workspace/reg-ass/literature/formalization/lit.bib"))
    (bibtex-completion-pdf-extension '(".pdf" ".djvu" ".ps"))
    (bibtex-completion-notes-path "/literature/notes")
    (bibtex-completion-pdf-open-function 'helm-open-file-externally)
    (bibtex-completion-additional-search-fields '(tags))
    (biblio-crossref-user-email-address "merlin.goettlinger@fau.de")
    :config
    (defun merlin-file-name (entry)
      (escape-symbols
       (concat
        (authors-format
         (or (bibtex-completion-get-value "author" entry)
             (bibtex-completion-get-value "editor" entry)))
        "_"
        (or (bibtex-completion-get-value "title" entry)
            (bibtex-completion-get-value "booktitle" entry)
            (bibtex-completion-get-value "journaltitle" entry)))))
    (setq bibtex-completion-format-citation-functions (cons '(org-mode . merlin-bibtex-completion-format-citation-org-aty-link-to-PDF) bibtex-completion-format-citation-functions))
    (defun merlin-insert-authors (keys)
      (insert (car (cl-loop
                    for key in keys
                    for entry = (bibtex-completion-get-entry key)
                    for author = (bibtex-completion-shorten-authors
                                  (or (bibtex-completion-get-value "author" entry)
                                      (bibtex-completion-get-value "editor" entry)))
                    collect author
                    ))))
    (helm-bibtex-helmify-action merlin-insert-authors helm-bibtex-insert-authors)
    (helm-add-action-to-source "Insert authors" 'helm-bibtex-insert-authors helm-source-bibtex 12)
    (defun merlin-bibtex-completion-format-citation-org-aty-link-to-PDF (keys) ; TODO: make paths relative to file
      "Formatter for org-links to PDF.  Link text loosely follows APA
format with included title.  Uses first matching PDF if several are available."
      (s-join ", " (cl-loop
                    for key in keys
                    for entry = (bibtex-completion-get-entry key)
                    for author = (bibtex-completion-shorten-authors
                                  (or (bibtex-completion-get-value "author" entry)
                                      (bibtex-completion-get-value "editor" entry)))
                    for title = (or (bibtex-completion-get-value "title" entry)
                                    (bibtex-completion-get-value "booktitle" entry)
                                    (bibtex-completion-get-value "journaltitle" entry))
                    for year = (or (bibtex-completion-get-value "year" entry)
                                   (car (split-string (bibtex-completion-get-value "date" entry "") "-")))
                    for pdf = (car (bibtex-completion-find-pdf key))
                    if pdf
                    collect (format "[[file:%s][%s - %s (%s)]]" pdf author title year)
                    else
                    collect (format "%s - %s (%s)" author title year))))

    (defun bibtex-completion-find-pdf-in-library (key-or-entry &optional find-additional)
      ;; redefined to handle custom naming scheme of files
      (let* ((entry (if (stringp key-or-entry)
                        (bibtex-completion-get-entry key-or-entry)
                      key-or-entry))
             (fn (merlin-file-name entry))
             (main-pdf (cl-loop
                        for dir in (-flatten bibtex-completion-library-path)
                        append (cl-loop
                                for ext in (-flatten bibtex-completion-pdf-extension)
                                collect (f-join dir (s-concat fn ext))))))
        (progn
          (message fn)
          (if find-additional
              (sort ; move main pdf on top of the list if needed
               (cl-loop
                for dir in (-flatten bibtex-completion-library-path)
                append (directory-files dir t
                                        (s-concat "^" (regexp-quote key)
                                                  ".*\\("
                                                  (mapconcat 'regexp-quote
                                                             (-flatten bibtex-completion-pdf-extension)
                                                             "\\|")
                                                  "\\)$")))
               (lambda (x y)
                 (and (member x main-pdf)
                      (not (member y main-pdf)))))
            (-flatten (-first 'f-file? main-pdf))))))
    (defun bibtex-completion-find-start-page (key)
      (let* ((entry (bibtex-completion-get-entry key))
             (pages (bibtex-completion-get-value "pages" entry))
             (start-page (bibtex-completion-get-value "startpage" entry)))
        (if start-page
            start-page
          (if pages
              (car (s-split "-" pages))
            "1")))
      )
    (defun bibtex-completion-pdf-find-offset (pdf start-page)
      (if (< (string-to-number start-page) (pdf-info-number-of-pages pdf))
          start-page
        "1")
      )
    (defun bibtex-completion-open-pdf (keys &optional fallback-action)
      ;; redefined to handle journal pdfs
      (dolist (key keys)
        (let ((pdf (bibtex-completion-find-pdf key bibtex-completion-find-additional-pdfs))
              (start-page (bibtex-completion-find-start-page key)))
          (cond
           ((> (length pdf) 1)
            (let* ((pdf (f-uniquify-alist pdf))
                   (choice (completing-read "File to open: " (mapcar 'cdr pdf) nil t))
                   (file (car (rassoc choice pdf))))
              (funcall bibtex-completion-pdf-open-function file)))
           (pdf
            (progn (funcall 'find-file (car pdf))
                   (when (derived-mode-p 'pdf-view-mode)
                     (pdf-view-goto-label (bibtex-completion-pdf-find-offset (car pdf) start-page)))))
           (fallback-action
            (funcall fallback-action (list key)))
           (t
            (message "No PDF(s) found for this entry: %s"
                     key))))))

    (defun bibtex-completion-make-bibtex (key)
      (let* ((entry (bibtex-completion-get-entry key))
             (entry-type (downcase (bibtex-completion-get-value "=type=" entry))))
        (format "@%s{%s,\n%s}\n"
                entry-type key
                (cl-loop
                 for field in entry
                 for name = (car field)
                 for value = (cdr field)
                 unless (member name
                                (append (-map (lambda (it) (if (symbolp it) (symbol-name it) it))
                                              bibtex-completion-no-export-fields)
                                        '("=type=" "=key=" "=has-pdf=" "=has-note=" "crossref")))
                 unless (and (bibtex-completion-get-value "=doi=" entry) (s-equals? name "=url=") (string-match (rx anything "doi.org" anything) value))
                 concat
                 (format "  %s = {%s},\n" name value)))))
    ;; resolve ebib links without using ebib
    ;; (org-link-set-parameters "ebib" :follow (lambda (key) (funcall bibtex-completion-pdf-open-function (car (bibtex-completion-find-pdf-in-library key)))))
    )
    :bind (("C-x C-b"   . helm-buffers-list)
           ("M-x"       . helm-M-x)
           ("C-x b"     . helm-mini)
           ("C-x C-f"   . helm-find-files)
           ("C-h a"     . helm-apropos)
           ("C-x C-r"   . helm-resume)
           ("C-s"       . helm-occur)
           ("C-S-s"     . helm-grep-do-git-grep)
           ("C-x C-d"   . helm-browse-project)
           ("C-x C-n"   . helm-imenu)
           ("C-x 8 RET" . helm-ucs)
           ("C-f"       . helm-bookmarks)
           :map helm-find-files-map
           ("C-<left>" . helm-find-files-up-one-level)
           ("C-<right>" . helm-ff-RET)
           :map helm-generic-files-map
           ("C-<left>" . helm-find-files-up-one-level))
  :hook
  (helm-before-initialize-hook . (lambda () (when helm-source-occur ;otherwise this breaks helm before helm-occur was called once TODO: there probably is a better way
                                              (helm-attrset 'follow 1 helm-source-occur)))))


(use-package company
  :custom
  (company-idle-delay 0)
  (company-minimum-prefix-length 2)
  :config
  ;; disables TAB in company-mode, freeing it for yasnippet
  (define-key company-active-map [tab] nil)
  (use-package company-quickhelp ;; Automatically display help pages for company completions
    :after company
    :config (company-quickhelp-mode))
  (use-package company-math ;; Company backend for math symbols
    :after company
    :config (add-to-list 'company-backends 'company-math-symbols-latex))
  (use-package company-box
    :after company
    :hook company-mode-hook)
  :config (global-company-mode))

(use-package yasnippet
  :config (yas-global-mode)
  :init
  (use-package yasnippet-classic-snippets)
  (use-package yasnippet-snippets)
  (use-package yatemplate :disabled :url https://github.com/mineo/yatemplate) ;TODO learn
  )

(use-package pdf-tools ;; Better pdf viewer
  :hook ((pdf-view-mode-hook . auto-revert-mode)
         (pdf-view-mode-hook . pdf-view-auto-slice-minor-mode)
         (pdf-view-mode-hook . pdf-view-midnight-minor-mode))
  :config
  (pdf-tools-install t)
  (use-package saveplace-pdf-view ;; resuming position in open pdfs utilizing emacs builtin functionality
    :after pdf-tools
    :config (save-place-mode))
  :bind (:map pdf-view-mode-map
         ("C-s" . isearch-forward)
         ("q" . kill-buffer-and-window)
         ("Q" . kill-this-buffer)))

(use-package magit
  :commands magit-status magit-blame
  :config
  (use-package magit-lfs) ;; git-lfs in magit
  ;; git config --add magit.extension svn
  (use-package magit-svn) ;; git-svn in magit
  (use-package magit-delta ;; use delta for prettier diffs
    :hook magit-mode-hook)
  (use-package forge) ;; support git forges
  :hook (magit-diff-mode-hook . visual-line-mode)
  :bind
  (("C-x f" . magit-file-dispatch)))

(use-package lsp-mode)
(use-package lsp-ui)

(use-package haskell-mode
  :custom
  (haskell-tags-on-save t)
  (haskell-hoogle-url "http://localhost:8080/?hoogle=%s")
  :config
  (use-package lsp-haskell)

  (defun load-haskell-prettify () (setq prettify-symbols-alist (append prettify-symbols-alist haskell-prettify-symbols-alist)))
  (defun my-helm-hoogle-candidates (&optional request-prefix)
    (let* ((pattern (or (and request-prefix
                             (concat request-prefix
                                     " " helm-pattern))
                        helm-pattern))
           (url (concat (format haskell-hoogle-url pattern) "&mode=json"))
           (name (url-file-local-copy url)))
      (let (candidates)
        (progn
          (let* ((json-object-type 'hash-table)
                 (json-array-type 'list)
                 (json-key-type 'string)
                 (json (json-read-file name)))
            (dolist (cand json) (push (list
                                       (replace-regexp-in-string (rx "<" (1+ (not (any ?>))) ">") ""
                                                                 (replace-regexp-in-string "&gt;" ">"
                                                                                           (replace-regexp-in-string "&lt;" "<"
                                                                                                                     (gethash "item" cand))))
                                       (gethash "doc" cand)
                                       (let ((u (gethash "url" cand)))
                                         (if (s-suffix? ".html" u) u (concat u "index.html")))) candidates)))
          (nreverse candidates)))))
  (defun helm-hoogle ()
    (interactive)
    (helm :sources
          (helm-build-sync-source "Hoogle"
                                  :candidates #'my-helm-hoogle-candidates
                                  :action '(("Lookup Entry" . (lambda (cand) (browse-url (car (cdr cand))))))
                                  :filtered-candidate-transformer (lambda (candidates source) candidates)
                                  :volatile t)
          :prompt "Hoogle: "
          :buffer "*Hoogle search*")))

(use-package idris2-mode
  :custom
  (idris2-stay-in-current-window-on-compiler-error t))

(use-package tex
  :custom
  (TeX-default-mode 'LaTeX-mode)
  (TeX-view-program-selection '((output-pdf "Sioyek")
                                (output-html "xdg-open")))
  (TeX-auto-save t "Save the parsed state for speed")
  (TeX-parse-self t "Parse the file automatically")
  (TeX-source-correlate-mode t "Allways generate synctex data") ; do this via customize instead of broken hook
  (TeX-check-engine t "Check if the right tex engine is used")

  :hook
  (after-save-hook . (lambda (&rest IGNORED) (when (eq major-mode 'latex-mode) (TeX-command-run-all nil)))) ; auto compile after save
  :config
  (add-hook 'TeX-after-compilation-finished-functions 'TeX-revert-document-buffer) ;reload pdf after compile
  (use-package reftex
    :after tex
    :custom (reftex-plug-into-AUCTeX t)
    :hook
    (LaTeX-mode-hook . turn-on-reftex)
    :bind
    (:map LaTeX-mode-map
     ("C-c C-i" . reftex-index-phrase-selection-or-word)
     ("C-c C-r" . reftex-reference)))

  (use-package company-auctex
    :after tex company
    :config (company-auctex-init))

  (use-package auctex-latexmk
    :after tex
    :config (auctex-latexmk-setup)))

(use-package tuareg)
(use-package merlin
  :custom
  (merlin-command "ocamlmerlin")
  :hook tuareg-mode-hook
  :after tuareg
  :bind (:map merlin-mode-map
         ("M-." . merlin-locate)
         ("M-," . merlin-pop-stack)))

(use-package nix-mode
  :config
  (use-package nix-update
    :after nix-mode
    :bind (:map nix-mode-map
           ("C-. u" . nix-update-fetch)))
  (use-package nixos-options
    :config
    (add-to-list 'company-backends 'company-nixos-options)))
(use-package csv-mode)
(use-package json-mode)
(use-package sh-script
  :hook (sh-mode-hook . flycheck-mode))

(use-package emacs-refactor
  :bind (:map lisp-mode-map
         ("C-RET" . emr-show-refactor-menu)))

(use-package lean-mode)
(use-package company-lean)
(use-package helm-lean)

(use-package rust-mode
  :hook (rust-mode-hook . lsp)
  :bind (:map rust-mode-map
         ("C-c C-c" . rust-compile)
         ("C-c C-x" . rust-run)))

(use-package cqp :disabled t
  :config (byteload "~/.emacs.d/cqp"))
(use-package cqp-annot :disabled t
  :config (byteload "~/.emacs.d/cqp-annot"))

(use-package elm-mode)

(use-package python-mode)

(use-package nushell-mode)

(use-package editorconfig
  :config (editorconfig-mode 1))
