{ pkgs, ... }:

let 
  mymacs = pkgs.emacsWithPackagesFromUsePackage {
    config = ./emacs.el;
    defaultInitFile = true; 
    alwaysEnsure = true;
    extraEmacsPackages = epkgs: [];
  };
in
  {
    nixpkgs.overlays = [
      (import (builtins.fetchTarball {
        url = "https://github.com/nix-community/emacs-overlay/archive/master.tar.gz";
      }))
    ];
    environment.systemPackages = with pkgs; [
      mymacs
      sqlite
      clang # some emacs packages require EmacsSQLite which needs a C compiler
      openssl # for gnus smime
    ];
    services = {
      emacs = {
        enable = true;
        install = true;
        defaultEditor = true;
        package = mymacs;
      };
    };
    fonts = {
      packages = with pkgs; [
        source-code-pro
        source-sans-pro
        source-serif-pro
        emacs-all-the-icons-fonts
        hack-font
        symbola
      ];
    };

    environment.shellAliases = let
      ec = "${mymacs.out}/bin/emacsclient -c -n";
    in {
      em = ec + " -nw";
      eq = "${mymacs.out}/bin/emacs -nw -q";
    };
  }
