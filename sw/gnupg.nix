{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    gnupg
  ];

  programs = {
    gnupg.agent = {
      enable = true;
      enableBrowserSocket = true;
      enableSSHSupport = true;
      pinentryPackage = pkgs.pinentry-gtk2;
    };
  };

  environment.shellInit = ''
    if ( ${pkgs.gnupg.out}/bin/gpg-connect-agent /bye ); then ${pkgs.gnupg.out}/bin/gpg-connect-agent updatestartuptty /bye > /dev/null else; fi
  '';

  services = {
    keybase.enable = true;
  };
}
