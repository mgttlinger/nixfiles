{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    python3Packages.binwalk

    radare2 radare2-cutter

    wireshark-qt ddrescue
  ];
}
