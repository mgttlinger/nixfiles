{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    light
  ];

  users.users.merlin.extraGroups = [ "video" ];

  programs = {
    light.enable = true;
  };
}
