{ pkgs, ... }:

let
  unstable = import <nixos-unstable> { };
in
{
  imports = [ <nixos-unstable/nixos/modules/services/misc/ollama.nix>];

  services = {
      ollama = {
        enable = true;
      };
    };
    environment.systemPackages = [
      unstable.ollama
    ];
  }
