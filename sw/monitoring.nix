{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    htop # process monitoring
    iotop bmon
  ];

  programs.iotop.enable = true;
}
