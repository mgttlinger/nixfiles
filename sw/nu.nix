{pkgs, ...}:

{
  environment.systemPackages = with pkgs; with nushellPlugins; [ nushellFull nufmt nu_scripts gstat net query formats ];
}
