{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    numix-gtk-theme numix-icon-theme
  ];
}
