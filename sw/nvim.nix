{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    neovim gnvim
  ];

  environment.shellAliases = {
    vv = "${pkgs.neovim.out}/bin/nvim -R";
  };
}
