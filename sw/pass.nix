{ pkgs, ...}:
{
  imports = [
    ./gnupg.nix
  ];
  environment.systemPackages = with pkgs; [
    pass browserpass
  ];

  programs = {
    browserpass.enable = true;
  };
}
