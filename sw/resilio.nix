{
  services.resilio = {
    enable = true;
    deviceName = networking.hostName;
    enableWebUI = true;
  };

  users.users.merlin.extraGroups = [ "rslsync" ];
}
