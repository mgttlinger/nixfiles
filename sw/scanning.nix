{ pkgs, ... }:

  {

    #pkgs.epsonscan2.override { withNonFreePlugins = true; };

    environment.systemPackages = with pkgs; [
      epsonscan2

      gscan2pdf
    ];

    hardware.sane = {
      extraBackends = with pkgs; [ epsonscan2 ];
      enable = true;
    };
  }
