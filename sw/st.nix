{ pkgs, ... }:

let
  anysize = builtins.fetchurl {
    url = "https://st.suckless.org/patches/anysize/st-anysize-0.8.1.diff";
  };
  scrollback = builtins.fetchurl {
    url = "https://st.suckless.org/patches/scrollback/st-scrollback-0.8.4.diff";
  };
  scrollback-mouse = builtins.fetchurl {
    url = "https://st.suckless.org/patches/scrollback/st-scrollback-mouse-20191024-a2c479c.diff";
  };
  no-bold = builtins.fetchurl {
    url = "https://st.suckless.org/patches/solarized/st-no_bold_colors-20170623-b331da5.diff";
  };
  w3m = builtins.fetchurl {
    url = "https://st.suckless.org/patches/w3m/st-w3m-0.8.3.diff";
  };
in
{
  environment.systemPackages = with pkgs; [
    st
  ];

  fonts = {
    fonts = with pkgs; [
      source-code-pro # my moe patch uses that
    ];
  };

  nixpkgs.config.st.patches = [
    ./st-moe.diff anysize scrollback scrollback-mouse w3m
  ];
}
