{ pkgs, ... }:

let

in
{
  environment.systemPackages = with pkgs; [
    surf wget curl
  ];

  nixpkgs.config.surf.patches = [

  ];
}
