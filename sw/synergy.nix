{ pkgs, ... }:

{
  services.synergy.server = {
    enable = true;
  };

  environment.systemPackages = with pkgs; [
    synergy
  ];
}
