{ pkgs, ... }:

let
  unstable = import <nixos-unstable> { };
in
{
  environment.systemPackages = with pkgs; [
    git-lfs subversion
    gitAndTools.git-extras
    gitAndTools.gitFull
    unstable.gitAndTools.delta
    pijul
  ];

  programs = {
    zsh = {
      shellAliases = {
        svnc = "${pkgs.subversion.out}/bin/svn status | ${pkgs.silver-searcher.out}/bin/ag \"^M\"";
      };
    };
  };
}
