{ pkgs, ... }:

{
  virtualisation.virtualbox.host = {
    enable = true;
  };

  users.users.merlin.extraGroups = [ "vboxusers" ];
}
