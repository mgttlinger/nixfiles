{ pkgs, ... }:

let
  myvscode = pkgs.vscode-with-extensions.override {
    vscodeExtensions = with pkgs.vscode-extensions; [
      james-yu.latex-workshop
    ];
  };
in
  {
    environment.systemPackages = with pkgs; [
      myvscode
    ];
    fonts = {
      packages = with pkgs; [
        source-code-pro
        source-sans-pro
        source-serif-pro
        hack-font
        symbola
      ];
    };
  }
