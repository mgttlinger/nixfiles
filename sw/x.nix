{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    polybar arandr
  ];

  services = {
    # Enable the X11 windowing system.
    xserver = {
      enable = true;
      desktopManager.xterm.enable = false;
      desktopManager.wallpaper.mode = "fill";
    };
    libinput = {
      enable = true;
      touchpad = {
        tapping = false;
        clickMethod = "clickfinger"; # Two finger right click
        scrollMethod = "twofinger";
        #accelSpeed = "0.05";
      };
    };
  };
}
