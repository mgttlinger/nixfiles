{ pkgs, ... }:

{
  imports = [
    ./x.nix
  ];
  environment.systemPackages = with pkgs; [
    xmonad-log
  ];

  services = {
    xserver = {
      windowManager.xmonad = {
        enable = true;
        enableContribAndExtras = true;
        extraPackages = haskellPackages: [
          haskellPackages.dbus
          haskellPackages.xmonad-utils
          haskellPackages.xmonad-volume
        ];
      };
    };
    displayManager.defaultSession = "none+xmonad";
  };

  nixpkgs.config = {
    packageOverrides = pkgs: {
      polybar = pkgs.polybar.override {
        githubSupport = true;
        mpdSupport = true;
        pulseSupport = true;
      };
    };
  };

  systemd.user.services = {
    polybar = {
      description = "Polybar";
      enable = true;
      path = with pkgs; [ xmonad-log
                          bash
                          playerctl
                       ];
      serviceConfig = {
        ExecStart = "${pkgs.polybar.out}/bin/polybar example";
        ExecReload = "${pkgs.coreutils.out}/bin/kill -HUP $MAINPID";
        PassEnvironment = "DISPLAY";
        KillMode = "control-group"; # upstream recommends process
        Restart = "on-failure";
        PrivateTmp = true;
        ProtectSystem = "full";
        Nice = 10;
      };
      after = ["graphical.target"];
      wantedBy = ["graphical.target"];
    };
  };
}
