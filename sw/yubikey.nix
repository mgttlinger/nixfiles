{ pkgs, ... }:

{
  imports = [
    ./gnupg.nix
  ];
  environment.systemPackages = with pkgs; [
    yubikey-personalization
  ];



  services = {
    udev = {
      enable = true;
      extraRules = ''
        ATTRS{idVendor}=="1050", ATTRS{idProduct}=="0010|0110|0111|0114|0116|0401|0403|0405|0407|0410", TAG+="systemd", SYMLINK+="yubikey"
      '';
      packages = with pkgs; [ yubikey-personalization ]; # For yubikey smartcard
    };
    pcscd.enable = true; # For smartcard
  };
}
