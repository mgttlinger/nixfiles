{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    zsh-completions nix-zsh-completions zsh-syntax-highlighting
    inotify-tools # file watching
  ];

  programs = {
    zsh = {
      enable = true;
      enableCompletion = true;
      syntaxHighlighting = {
        enable = true;
        highlighters = ["main" "brackets" "pattern"];
      };
    };
  };
}
